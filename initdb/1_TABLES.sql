CREATE TABLE temperature
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE humidite
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE pression
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE temperatureHome
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE humiditeHome
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE pressionHome
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE temperatureSchool
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE humiditeSchool
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);

CREATE TABLE pressionSchool
(
    id    INT PRIMARY KEY NULL,
    time  TEXT            null,
    value FLOAT           null
);
