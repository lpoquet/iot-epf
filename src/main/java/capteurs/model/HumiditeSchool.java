package capteurs.model;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

/**
 *
 */

@Entity
@Table(name = "humiditeSchool")
public class HumiditeSchool {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String time;
    private String value;

    public HumiditeSchool(String time, String value) {
        this.time = time;
        this.value = value;
    }

    public HumiditeSchool() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        long millis = Long.parseLong(time);
        Date date = new Date(millis*1000);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE,MMMM d,yyyy h:mm,a", Locale.FRENCH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "HumiditeSchool{" +
                "id=" + id +
                ", nom='" + time + '\'' +
                ", valeur='" + value + '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, value);
    }

}
