package capteurs;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import capteurs.DAO.*;
import capteurs.model.*;


@Controller
@RequestMapping("/")
public class LibController {

    private final TemperatureDAO tempDAO;
    private final HumiditeDAO humDAO;
    private final PressionDAO presDAO;
    private final TemperatureHomeDAO tempHDAO;
    private final HumiditeHomeDAO humHDAO;
    private final PressionHomeDAO presHDAO;
    private final TemperatureSchoolDAO tempSDAO;
    private final HumiditeSchoolDAO humSDAO;
    private final PressionSchoolDAO presSDAO;

    public LibController(TemperatureDAO tempDAO, HumiditeDAO humDAO, PressionDAO presDAO, TemperatureHomeDAO tempHDAO, HumiditeHomeDAO humHDAO, PressionHomeDAO presHDAO, TemperatureSchoolDAO tempSDAO, HumiditeSchoolDAO humSDAO, PressionSchoolDAO presSDAO) {
        this.tempDAO = tempDAO;
        this.humDAO = humDAO;
        this.presDAO = presDAO;
        this.tempHDAO = tempHDAO;
        this.humHDAO = humHDAO;
        this.presHDAO = presHDAO;
        this.tempSDAO = tempSDAO;
        this.humSDAO = humSDAO;
        this.presSDAO = presSDAO;
    }

    @GetMapping("home")
    public String homePage(Model m) {
        Temperature[] temps = new Temperature[]{tempDAO.findAllById((int) tempDAO.count() - 19),
                tempDAO.findAllById((int) tempDAO.count() - 18), tempDAO.findAllById((int) tempDAO.count() - 17),
                tempDAO.findAllById((int) tempDAO.count() - 16), tempDAO.findAllById((int) tempDAO.count() - 15),
                tempDAO.findAllById((int) tempDAO.count() - 14), tempDAO.findAllById((int) tempDAO.count() - 13),
                tempDAO.findAllById((int) tempDAO.count() - 12), tempDAO.findAllById((int) tempDAO.count() - 11),
                tempDAO.findAllById((int) tempDAO.count() - 10), tempDAO.findAllById((int) tempDAO.count() - 9),
                tempDAO.findAllById((int) tempDAO.count() - 8), tempDAO.findAllById((int) tempDAO.count() - 7),
                tempDAO.findAllById((int) tempDAO.count() - 6), tempDAO.findAllById((int) tempDAO.count() - 5),
                tempDAO.findAllById((int) tempDAO.count() - 4), tempDAO.findAllById((int) tempDAO.count() - 3),
                tempDAO.findAllById((int) tempDAO.count() - 2), tempDAO.findAllById((int) tempDAO.count() - 1),
                tempDAO.findAllById((int) tempDAO.count())
        };
        String[] tempsT = new String[] { tempDAO.findAllById((int) tempDAO.count() - 19).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 18).getTime(), tempDAO.findAllById((int) tempDAO.count() - 17).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 16).getTime(), tempDAO.findAllById((int) tempDAO.count() - 15).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 14).getTime(), tempDAO.findAllById((int) tempDAO.count() - 13).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 12).getTime(), tempDAO.findAllById((int) tempDAO.count() - 11).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 10).getTime(), tempDAO.findAllById((int) tempDAO.count() - 9).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 8).getTime(), tempDAO.findAllById((int) tempDAO.count() - 7).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 6).getTime(), tempDAO.findAllById((int) tempDAO.count() - 5).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 4).getTime(), tempDAO.findAllById((int) tempDAO.count() - 3).getTime(),
                tempDAO.findAllById((int) tempDAO.count() - 2).getTime(), tempDAO.findAllById((int) tempDAO.count() - 1).getTime(),
                tempDAO.findAllById((int) tempDAO.count()).getTime()
        };
        String[] tempsV = new String[] { tempDAO.findAllById((int) tempDAO.count() - 19).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 18).getValue(), tempDAO.findAllById((int) tempDAO.count() - 17).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 16).getValue(), tempDAO.findAllById((int) tempDAO.count() - 15).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 14).getValue(), tempDAO.findAllById((int) tempDAO.count() - 13).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 12).getValue(), tempDAO.findAllById((int) tempDAO.count() - 11).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 10).getValue(), tempDAO.findAllById((int) tempDAO.count() - 9).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 8).getValue(), tempDAO.findAllById((int) tempDAO.count() - 7).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 6).getValue(), tempDAO.findAllById((int) tempDAO.count() - 5).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 4).getValue(), tempDAO.findAllById((int) tempDAO.count() - 3).getValue(),
                tempDAO.findAllById((int) tempDAO.count() - 2).getValue(), tempDAO.findAllById((int) tempDAO.count() - 1).getValue(),
                tempDAO.findAllById((int) tempDAO.count()).getValue()
        };
        m.addAttribute("temps", temps);
        m.addAttribute("tempsT", tempsT);
        m.addAttribute("tempsV", tempsV);
        Humidite[] hum = new Humidite[]{humDAO.findAllById((int) humDAO.count() - 19),
                humDAO.findAllById((int) humDAO.count() - 18), humDAO.findAllById((int) humDAO.count() - 17),
                humDAO.findAllById((int) humDAO.count() - 16), humDAO.findAllById((int) humDAO.count() - 15),
                humDAO.findAllById((int) humDAO.count() - 14), humDAO.findAllById((int) humDAO.count() - 13),
                humDAO.findAllById((int) humDAO.count() - 12), humDAO.findAllById((int) humDAO.count() - 11),
                humDAO.findAllById((int) humDAO.count() - 10), humDAO.findAllById((int) humDAO.count() - 9),
                humDAO.findAllById((int) humDAO.count() - 8), humDAO.findAllById((int) humDAO.count() - 7),
                humDAO.findAllById((int) humDAO.count() - 6), humDAO.findAllById((int) humDAO.count() - 5),
                humDAO.findAllById((int) humDAO.count() - 4), humDAO.findAllById((int) humDAO.count() - 3),
                humDAO.findAllById((int) humDAO.count() - 2), humDAO.findAllById((int) humDAO.count() - 1),
                humDAO.findAllById((int) humDAO.count())
        };
        String[] humT = new String[] { humDAO.findAllById((int) humDAO.count() - 19).getTime(),
                humDAO.findAllById((int) humDAO.count() - 18).getTime(), humDAO.findAllById((int) humDAO.count() - 17).getTime(),
                humDAO.findAllById((int) humDAO.count() - 16).getTime(), humDAO.findAllById((int) humDAO.count() - 15).getTime(),
                humDAO.findAllById((int) humDAO.count() - 14).getTime(), humDAO.findAllById((int) humDAO.count() - 13).getTime(),
                humDAO.findAllById((int) humDAO.count() - 12).getTime(), humDAO.findAllById((int) humDAO.count() - 11).getTime(),
                humDAO.findAllById((int) humDAO.count() - 10).getTime(), humDAO.findAllById((int) humDAO.count() - 9).getTime(),
                humDAO.findAllById((int) humDAO.count() - 8).getTime(), humDAO.findAllById((int) humDAO.count() - 7).getTime(),
                humDAO.findAllById((int) humDAO.count() - 6).getTime(), humDAO.findAllById((int) humDAO.count() - 5).getTime(),
                humDAO.findAllById((int) humDAO.count() - 4).getTime(), humDAO.findAllById((int) humDAO.count() - 3).getTime(),
                humDAO.findAllById((int) humDAO.count() - 2).getTime(), humDAO.findAllById((int) humDAO.count() - 1).getTime(),
                humDAO.findAllById((int) humDAO.count()).getTime()
        };
        String[] humV = new String[] { humDAO.findAllById((int) humDAO.count() - 19).getValue(),
                humDAO.findAllById((int) humDAO.count() - 18).getValue(), humDAO.findAllById((int) humDAO.count() - 17).getValue(),
                humDAO.findAllById((int) humDAO.count() - 16).getValue(), humDAO.findAllById((int) humDAO.count() - 15).getValue(),
                humDAO.findAllById((int) humDAO.count() - 14).getValue(), humDAO.findAllById((int) humDAO.count() - 13).getValue(),
                humDAO.findAllById((int) humDAO.count() - 12).getValue(), humDAO.findAllById((int) humDAO.count() - 11).getValue(),
                humDAO.findAllById((int) humDAO.count() - 10).getValue(), humDAO.findAllById((int) humDAO.count() - 9).getValue(),
                humDAO.findAllById((int) humDAO.count() - 8).getValue(), humDAO.findAllById((int) humDAO.count() - 7).getValue(),
                humDAO.findAllById((int) humDAO.count() - 6).getValue(), humDAO.findAllById((int) humDAO.count() - 5).getValue(),
                humDAO.findAllById((int) humDAO.count() - 4).getValue(), humDAO.findAllById((int) humDAO.count() - 3).getValue(),
                humDAO.findAllById((int) humDAO.count() - 2).getValue(), humDAO.findAllById((int) humDAO.count() - 1).getValue(),
                humDAO.findAllById((int) humDAO.count()).getValue()
        };
        m.addAttribute("hum", hum);
        m.addAttribute("humT", humT);
        m.addAttribute("humV", humV);
        Pression[] pres = new Pression[]{presDAO.findAllById((int) presDAO.count() - 19),
                presDAO.findAllById((int) presDAO.count() - 18), presDAO.findAllById((int) presDAO.count() - 17),
                presDAO.findAllById((int) presDAO.count() - 16), presDAO.findAllById((int) presDAO.count() - 15),
                presDAO.findAllById((int) presDAO.count() - 14), presDAO.findAllById((int) presDAO.count() - 13),
                presDAO.findAllById((int) presDAO.count() - 12), presDAO.findAllById((int) presDAO.count() - 11),
                presDAO.findAllById((int) presDAO.count() - 10), presDAO.findAllById((int) presDAO.count() - 9),
                presDAO.findAllById((int) presDAO.count() - 8), presDAO.findAllById((int) presDAO.count() - 7),
                presDAO.findAllById((int) presDAO.count() - 6), presDAO.findAllById((int) presDAO.count() - 5),
                presDAO.findAllById((int) presDAO.count() - 4), presDAO.findAllById((int) presDAO.count() - 3),
                presDAO.findAllById((int) presDAO.count() - 2), presDAO.findAllById((int) presDAO.count() - 1),
                presDAO.findAllById((int) presDAO.count())
        };
        String[] presT = new String[] { presDAO.findAllById((int) presDAO.count() - 19).getTime(),
                presDAO.findAllById((int) presDAO.count() - 18).getTime(), presDAO.findAllById((int) presDAO.count() - 17).getTime(),
                presDAO.findAllById((int) presDAO.count() - 16).getTime(), presDAO.findAllById((int) presDAO.count() - 15).getTime(),
                presDAO.findAllById((int) presDAO.count() - 14).getTime(), presDAO.findAllById((int) presDAO.count() - 13).getTime(),
                presDAO.findAllById((int) presDAO.count() - 12).getTime(), presDAO.findAllById((int) presDAO.count() - 11).getTime(),
                presDAO.findAllById((int) presDAO.count() - 10).getTime(), presDAO.findAllById((int) presDAO.count() - 9).getTime(),
                presDAO.findAllById((int) presDAO.count() - 8).getTime(), presDAO.findAllById((int) presDAO.count() - 7).getTime(),
                presDAO.findAllById((int) presDAO.count() - 6).getTime(), presDAO.findAllById((int) presDAO.count() - 5).getTime(),
                presDAO.findAllById((int) presDAO.count() - 4).getTime(), presDAO.findAllById((int) presDAO.count() - 3).getTime(),
                presDAO.findAllById((int) presDAO.count() - 2).getTime(), presDAO.findAllById((int) presDAO.count() - 1).getTime(),
                presDAO.findAllById((int) presDAO.count()).getTime()
        };
        String[] presV = new String[] { presDAO.findAllById((int) presDAO.count() - 19).getValue(),
                presDAO.findAllById((int) presDAO.count() - 18).getValue(), presDAO.findAllById((int) presDAO.count() - 17).getValue(),
                presDAO.findAllById((int) presDAO.count() - 16).getValue(), presDAO.findAllById((int) presDAO.count() - 15).getValue(),
                presDAO.findAllById((int) presDAO.count() - 14).getValue(), presDAO.findAllById((int) presDAO.count() - 13).getValue(),
                presDAO.findAllById((int) presDAO.count() - 12).getValue(), presDAO.findAllById((int) presDAO.count() - 11).getValue(),
                presDAO.findAllById((int) presDAO.count() - 10).getValue(), presDAO.findAllById((int) presDAO.count() - 9).getValue(),
                presDAO.findAllById((int) presDAO.count() - 8).getValue(), presDAO.findAllById((int) presDAO.count() - 7).getValue(),
                presDAO.findAllById((int) presDAO.count() - 6).getValue(), presDAO.findAllById((int) presDAO.count() - 5).getValue(),
                presDAO.findAllById((int) presDAO.count() - 4).getValue(), presDAO.findAllById((int) presDAO.count() - 3).getValue(),
                presDAO.findAllById((int) presDAO.count() - 2).getValue(), presDAO.findAllById((int) presDAO.count() - 1).getValue(),
                presDAO.findAllById((int) presDAO.count()).getValue()
        };
        m.addAttribute("pres", pres);
        m.addAttribute("presT", presT);
        m.addAttribute("presV", presV);
        return "home.html";
    }

    @GetMapping("values")
    public String valuesPage(Model m) {
        Temperature[] temps = new Temperature[] { tempDAO.findAllById((int) tempDAO.count() - 19),
                tempDAO.findAllById((int) tempDAO.count() - 18), tempDAO.findAllById((int) tempDAO.count() - 17),
                tempDAO.findAllById((int) tempDAO.count() - 16), tempDAO.findAllById((int) tempDAO.count() - 15),
                tempDAO.findAllById((int) tempDAO.count() - 14), tempDAO.findAllById((int) tempDAO.count() - 13),
                tempDAO.findAllById((int) tempDAO.count() - 12), tempDAO.findAllById((int) tempDAO.count() - 11),
                tempDAO.findAllById((int) tempDAO.count() - 10), tempDAO.findAllById((int) tempDAO.count() - 9),
                tempDAO.findAllById((int) tempDAO.count() - 8), tempDAO.findAllById((int) tempDAO.count() - 7),
                tempDAO.findAllById((int) tempDAO.count() - 6), tempDAO.findAllById((int) tempDAO.count() - 5),
                tempDAO.findAllById((int) tempDAO.count() - 4), tempDAO.findAllById((int) tempDAO.count() - 3),
                tempDAO.findAllById((int) tempDAO.count() - 2), tempDAO.findAllById((int) tempDAO.count() - 1),
                tempDAO.findAllById((int) tempDAO.count())
        };
        m.addAttribute("temps", temps);
        Humidite[] hums = new Humidite[] { humDAO.findAllById((int) humDAO.count() - 19),
                humDAO.findAllById((int) humDAO.count() - 18), humDAO.findAllById((int) humDAO.count() - 17),
                humDAO.findAllById((int) humDAO.count() - 16), humDAO.findAllById((int) humDAO.count() - 15),
                humDAO.findAllById((int) humDAO.count() - 14), humDAO.findAllById((int) humDAO.count() - 13),
                humDAO.findAllById((int) humDAO.count() - 12), humDAO.findAllById((int) humDAO.count() - 11),
                humDAO.findAllById((int) humDAO.count() - 10), humDAO.findAllById((int) humDAO.count() - 9),
                humDAO.findAllById((int) humDAO.count() - 8), humDAO.findAllById((int) humDAO.count() - 7),
                humDAO.findAllById((int) humDAO.count() - 6), humDAO.findAllById((int) humDAO.count() - 5),
                humDAO.findAllById((int) humDAO.count() - 4), humDAO.findAllById((int) humDAO.count() - 3),
                humDAO.findAllById((int) humDAO.count() - 2), humDAO.findAllById((int) humDAO.count() - 1),
                humDAO.findAllById((int) humDAO.count())
        };
        m.addAttribute("hums", hums);
        Pression[] pres = new Pression[] { presDAO.findAllById((int) presDAO.count() - 19),
                presDAO.findAllById((int) presDAO.count() - 18), presDAO.findAllById((int) presDAO.count() - 17),
                presDAO.findAllById((int) presDAO.count() - 16), presDAO.findAllById((int) presDAO.count() - 15),
                presDAO.findAllById((int) presDAO.count() - 14), presDAO.findAllById((int) presDAO.count() - 13),
                presDAO.findAllById((int) presDAO.count() - 12), presDAO.findAllById((int) presDAO.count() - 11),
                presDAO.findAllById((int) presDAO.count() - 10), presDAO.findAllById((int) presDAO.count() - 9),
                presDAO.findAllById((int) presDAO.count() - 8), presDAO.findAllById((int) presDAO.count() - 7),
                presDAO.findAllById((int) presDAO.count() - 6), presDAO.findAllById((int) presDAO.count() - 5),
                presDAO.findAllById((int) presDAO.count() - 4), presDAO.findAllById((int) presDAO.count() - 3),
                presDAO.findAllById((int) presDAO.count() - 2), presDAO.findAllById((int) presDAO.count() - 1),
                presDAO.findAllById((int) presDAO.count())
        };
        m.addAttribute("pres", pres);
        return "values.html";
    }

    @GetMapping("comparison")
    public String comparisonPage(Model m) {
        TemperatureHome[] tempsH = new TemperatureHome[]{tempHDAO.findAllById(1),
                tempHDAO.findAllById(2), tempHDAO.findAllById(3),
                tempHDAO.findAllById(4), tempHDAO.findAllById(5),
                tempHDAO.findAllById(6), tempHDAO.findAllById(7),
                tempHDAO.findAllById(8), tempHDAO.findAllById(9),
                tempHDAO.findAllById(10), tempHDAO.findAllById(11),
                tempHDAO.findAllById(12), tempHDAO.findAllById(13),
                tempHDAO.findAllById(14), tempHDAO.findAllById(15),
                tempHDAO.findAllById(16), tempHDAO.findAllById(17),
                tempHDAO.findAllById(18), tempHDAO.findAllById(19),
                tempHDAO.findAllById(20)
        };
        String[] tempsHT = new String[] { tempHDAO.findAllById(1).getTime(),
                tempHDAO.findAllById(2).getTime(), tempHDAO.findAllById(3).getTime(),
                tempHDAO.findAllById(4).getTime(), tempHDAO.findAllById(5).getTime(),
                tempHDAO.findAllById(6).getTime(), tempHDAO.findAllById(7).getTime(),
                tempHDAO.findAllById(8).getTime(), tempHDAO.findAllById(9).getTime(),
                tempHDAO.findAllById(10).getTime(), tempHDAO.findAllById(11).getTime(),
                tempHDAO.findAllById(12).getTime(), tempHDAO.findAllById(13).getTime(),
                tempHDAO.findAllById(14).getTime(), tempHDAO.findAllById(15).getTime(),
                tempHDAO.findAllById(16).getTime(), tempHDAO.findAllById(17).getTime(),
                tempHDAO.findAllById(18).getTime(), tempHDAO.findAllById(19).getTime(),
                tempHDAO.findAllById(20).getTime()
        };
        String[] tempsHV = new String[] { tempHDAO.findAllById(1).getValue(),
                tempHDAO.findAllById(2).getValue(), tempHDAO.findAllById(3).getValue(),
                tempHDAO.findAllById(4).getValue(), tempHDAO.findAllById(5).getValue(),
                tempHDAO.findAllById(6).getValue(), tempHDAO.findAllById(7).getValue(),
                tempHDAO.findAllById(8).getValue(), tempHDAO.findAllById(9).getValue(),
                tempHDAO.findAllById(10).getValue(), tempHDAO.findAllById(11).getValue(),
                tempHDAO.findAllById(12).getValue(), tempHDAO.findAllById(13).getValue(),
                tempHDAO.findAllById(14).getValue(), tempHDAO.findAllById(15).getValue(),
                tempHDAO.findAllById(16).getValue(), tempHDAO.findAllById(17).getValue(),
                tempHDAO.findAllById(18).getValue(), tempHDAO.findAllById(19).getValue(),
                tempHDAO.findAllById(20).getValue()
        };
        m.addAttribute("tempsH", tempsH);
        m.addAttribute("tempsHT", tempsHT);
        m.addAttribute("tempsHV", tempsHV);
        Humidite[] hum = new Humidite[]{humDAO.findAllById((int) humDAO.count() - 19),
                humDAO.findAllById((int) humDAO.count() - 18), humDAO.findAllById((int) humDAO.count() - 17),
                humDAO.findAllById((int) humDAO.count() - 16), humDAO.findAllById((int) humDAO.count() - 15),
                humDAO.findAllById((int) humDAO.count() - 14), humDAO.findAllById((int) humDAO.count() - 13),
                humDAO.findAllById((int) humDAO.count() - 12), humDAO.findAllById((int) humDAO.count() - 11),
                humDAO.findAllById((int) humDAO.count() - 10), humDAO.findAllById((int) humDAO.count() - 9),
                humDAO.findAllById((int) humDAO.count() - 8), humDAO.findAllById((int) humDAO.count() - 7),
                humDAO.findAllById((int) humDAO.count() - 6), humDAO.findAllById((int) humDAO.count() - 5),
                humDAO.findAllById((int) humDAO.count() - 4), humDAO.findAllById((int) humDAO.count() - 3),
                humDAO.findAllById((int) humDAO.count() - 2), humDAO.findAllById((int) humDAO.count() - 1),
                humDAO.findAllById((int) humDAO.count())
        };
        String[] humT = new String[] { humDAO.findAllById((int) humDAO.count() - 19).getTime(),
                humDAO.findAllById((int) humDAO.count() - 18).getTime(), humDAO.findAllById((int) humDAO.count() - 17).getTime(),
                humDAO.findAllById((int) humDAO.count() - 16).getTime(), humDAO.findAllById((int) humDAO.count() - 15).getTime(),
                humDAO.findAllById((int) humDAO.count() - 14).getTime(), humDAO.findAllById((int) humDAO.count() - 13).getTime(),
                humDAO.findAllById((int) humDAO.count() - 12).getTime(), humDAO.findAllById((int) humDAO.count() - 11).getTime(),
                humDAO.findAllById((int) humDAO.count() - 10).getTime(), humDAO.findAllById((int) humDAO.count() - 9).getTime(),
                humDAO.findAllById((int) humDAO.count() - 8).getTime(), humDAO.findAllById((int) humDAO.count() - 7).getTime(),
                humDAO.findAllById((int) humDAO.count() - 6).getTime(), humDAO.findAllById((int) humDAO.count() - 5).getTime(),
                humDAO.findAllById((int) humDAO.count() - 4).getTime(), humDAO.findAllById((int) humDAO.count() - 3).getTime(),
                humDAO.findAllById((int) humDAO.count() - 2).getTime(), humDAO.findAllById((int) humDAO.count() - 1).getTime(),
                humDAO.findAllById((int) humDAO.count()).getTime()
        };
        String[] humV = new String[] { humDAO.findAllById((int) humDAO.count() - 19).getValue(),
                humDAO.findAllById((int) humDAO.count() - 18).getValue(), humDAO.findAllById((int) humDAO.count() - 17).getValue(),
                humDAO.findAllById((int) humDAO.count() - 16).getValue(), humDAO.findAllById((int) humDAO.count() - 15).getValue(),
                humDAO.findAllById((int) humDAO.count() - 14).getValue(), humDAO.findAllById((int) humDAO.count() - 13).getValue(),
                humDAO.findAllById((int) humDAO.count() - 12).getValue(), humDAO.findAllById((int) humDAO.count() - 11).getValue(),
                humDAO.findAllById((int) humDAO.count() - 10).getValue(), humDAO.findAllById((int) humDAO.count() - 9).getValue(),
                humDAO.findAllById((int) humDAO.count() - 8).getValue(), humDAO.findAllById((int) humDAO.count() - 7).getValue(),
                humDAO.findAllById((int) humDAO.count() - 6).getValue(), humDAO.findAllById((int) humDAO.count() - 5).getValue(),
                humDAO.findAllById((int) humDAO.count() - 4).getValue(), humDAO.findAllById((int) humDAO.count() - 3).getValue(),
                humDAO.findAllById((int) humDAO.count() - 2).getValue(), humDAO.findAllById((int) humDAO.count() - 1).getValue(),
                humDAO.findAllById((int) humDAO.count()).getValue()
        };
        m.addAttribute("hum", hum);
        m.addAttribute("humT", humT);
        m.addAttribute("humV", humV);
        Pression[] pres = new Pression[]{presDAO.findAllById((int) presDAO.count() - 19),
                presDAO.findAllById((int) presDAO.count() - 18), presDAO.findAllById((int) presDAO.count() - 17),
                presDAO.findAllById((int) presDAO.count() - 16), presDAO.findAllById((int) presDAO.count() - 15),
                presDAO.findAllById((int) presDAO.count() - 14), presDAO.findAllById((int) presDAO.count() - 13),
                presDAO.findAllById((int) presDAO.count() - 12), presDAO.findAllById((int) presDAO.count() - 11),
                presDAO.findAllById((int) presDAO.count() - 10), presDAO.findAllById((int) presDAO.count() - 9),
                presDAO.findAllById((int) presDAO.count() - 8), presDAO.findAllById((int) presDAO.count() - 7),
                presDAO.findAllById((int) presDAO.count() - 6), presDAO.findAllById((int) presDAO.count() - 5),
                presDAO.findAllById((int) presDAO.count() - 4), presDAO.findAllById((int) presDAO.count() - 3),
                presDAO.findAllById((int) presDAO.count() - 2), presDAO.findAllById((int) presDAO.count() - 1),
                presDAO.findAllById((int) presDAO.count())
        };
        String[] presT = new String[] { presDAO.findAllById((int) presDAO.count() - 19).getTime(),
                presDAO.findAllById((int) presDAO.count() - 18).getTime(), presDAO.findAllById((int) presDAO.count() - 17).getTime(),
                presDAO.findAllById((int) presDAO.count() - 16).getTime(), presDAO.findAllById((int) presDAO.count() - 15).getTime(),
                presDAO.findAllById((int) presDAO.count() - 14).getTime(), presDAO.findAllById((int) presDAO.count() - 13).getTime(),
                presDAO.findAllById((int) presDAO.count() - 12).getTime(), presDAO.findAllById((int) presDAO.count() - 11).getTime(),
                presDAO.findAllById((int) presDAO.count() - 10).getTime(), presDAO.findAllById((int) presDAO.count() - 9).getTime(),
                presDAO.findAllById((int) presDAO.count() - 8).getTime(), presDAO.findAllById((int) presDAO.count() - 7).getTime(),
                presDAO.findAllById((int) presDAO.count() - 6).getTime(), presDAO.findAllById((int) presDAO.count() - 5).getTime(),
                presDAO.findAllById((int) presDAO.count() - 4).getTime(), presDAO.findAllById((int) presDAO.count() - 3).getTime(),
                presDAO.findAllById((int) presDAO.count() - 2).getTime(), presDAO.findAllById((int) presDAO.count() - 1).getTime(),
                presDAO.findAllById((int) presDAO.count()).getTime()
        };
        String[] presV = new String[] { presDAO.findAllById((int) presDAO.count() - 19).getValue(),
                presDAO.findAllById((int) presDAO.count() - 18).getValue(), presDAO.findAllById((int) presDAO.count() - 17).getValue(),
                presDAO.findAllById((int) presDAO.count() - 16).getValue(), presDAO.findAllById((int) presDAO.count() - 15).getValue(),
                presDAO.findAllById((int) presDAO.count() - 14).getValue(), presDAO.findAllById((int) presDAO.count() - 13).getValue(),
                presDAO.findAllById((int) presDAO.count() - 12).getValue(), presDAO.findAllById((int) presDAO.count() - 11).getValue(),
                presDAO.findAllById((int) presDAO.count() - 10).getValue(), presDAO.findAllById((int) presDAO.count() - 9).getValue(),
                presDAO.findAllById((int) presDAO.count() - 8).getValue(), presDAO.findAllById((int) presDAO.count() - 7).getValue(),
                presDAO.findAllById((int) presDAO.count() - 6).getValue(), presDAO.findAllById((int) presDAO.count() - 5).getValue(),
                presDAO.findAllById((int) presDAO.count() - 4).getValue(), presDAO.findAllById((int) presDAO.count() - 3).getValue(),
                presDAO.findAllById((int) presDAO.count() - 2).getValue(), presDAO.findAllById((int) presDAO.count() - 1).getValue(),
                presDAO.findAllById((int) presDAO.count()).getValue()
        };
        m.addAttribute("pres", pres);
        m.addAttribute("presT", presT);
        m.addAttribute("presV", presV);
//        TemperatureHome[] tempsH = new TemperatureHome[]{tempHDAO.findAllById((int) tempHDAO.count() - 19),
//                tempHDAO.findAllById((int) tempHDAO.count() - 18), tempHDAO.findAllById((int) tempHDAO.count() - 17),
//                tempHDAO.findAllById((int) tempHDAO.count() - 16), tempHDAO.findAllById((int) tempHDAO.count() - 15),
//                tempHDAO.findAllById((int) tempHDAO.count() - 14), tempHDAO.findAllById((int) tempHDAO.count() - 13),
//                tempHDAO.findAllById((int) tempHDAO.count() - 12), tempHDAO.findAllById((int) tempHDAO.count() - 11),
//                tempHDAO.findAllById((int) tempHDAO.count() - 10), tempHDAO.findAllById((int) tempHDAO.count() - 9),
//                tempHDAO.findAllById((int) tempHDAO.count() - 8), tempHDAO.findAllById((int) tempHDAO.count() - 7),
//                tempHDAO.findAllById((int) tempHDAO.count() - 6), tempHDAO.findAllById((int) tempHDAO.count() - 5),
//                tempHDAO.findAllById((int) tempHDAO.count() - 4), tempHDAO.findAllById((int) tempHDAO.count() - 3),
//                tempHDAO.findAllById((int) tempHDAO.count() - 2), tempHDAO.findAllById((int) tempHDAO.count() - 1),
//                tempHDAO.findAllById((int) tempHDAO.count())
//        };
//        String[] tempsHT = new String[] { tempHDAO.findAllById((int) tempHDAO.count() - 19).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 18).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 17).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 16).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 15).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 14).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 13).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 12).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 11).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 10).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 9).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 8).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 7).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 6).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 5).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 4).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 3).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 2).getTime(), tempHDAO.findAllById((int) tempHDAO.count() - 1).getTime(),
//                tempHDAO.findAllById((int) tempHDAO.count()).getTime()
//        };
//        String[] tempsHV = new String[] { tempHDAO.findAllById((int) tempHDAO.count() - 19).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 18).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 17).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 16).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 15).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 14).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 13).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 12).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 11).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 10).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 9).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 8).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 7).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 6).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 5).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 4).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 3).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count() - 2).getValue(), tempHDAO.findAllById((int) tempHDAO.count() - 1).getValue(),
//                tempHDAO.findAllById((int) tempHDAO.count()).getValue()
//        };
//        m.addAttribute("tempsH", tempsH);
//        m.addAttribute("tempsHT", tempsHT);
//        m.addAttribute("tempsHV", tempsHV);
//        String[] tempsST = new String[] { tempSDAO.findAllById((int) tempSDAO.count() - 19).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 18).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 17).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 16).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 15).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 14).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 13).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 12).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 11).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 10).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 9).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 8).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 7).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 6).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 5).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 4).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 3).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 2).getTime(), tempSDAO.findAllById((int) tempSDAO.count() - 1).getTime(),
//                tempSDAO.findAllById((int) tempSDAO.count()).getTime()
//        };
//        String[] tempsSV = new String[] { tempSDAO.findAllById((int) tempSDAO.count() - 19).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 18).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 17).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 16).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 15).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 14).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 13).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 12).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 11).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 10).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 9).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 8).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 7).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 6).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 5).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 4).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 3).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count() - 2).getValue(), tempSDAO.findAllById((int) tempSDAO.count() - 1).getValue(),
//                tempSDAO.findAllById((int) tempSDAO.count()).getValue()
//        };
//        m.addAttribute("tempsST", tempsST);
//        m.addAttribute("tempsSV", tempsSV);
//        String[] humHT = new String[] { humHDAO.findAllById((int) humHDAO.count() - 19).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 18).getTime(), humHDAO.findAllById((int) humHDAO.count() - 17).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 16).getTime(), humHDAO.findAllById((int) humHDAO.count() - 15).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 14).getTime(), humHDAO.findAllById((int) humHDAO.count() - 13).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 12).getTime(), humHDAO.findAllById((int) humHDAO.count() - 11).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 10).getTime(), humHDAO.findAllById((int) humHDAO.count() - 9).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 8).getTime(), humHDAO.findAllById((int) humHDAO.count() - 7).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 6).getTime(), humHDAO.findAllById((int) humHDAO.count() - 5).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 4).getTime(), humHDAO.findAllById((int) humHDAO.count() - 3).getTime(),
//                humHDAO.findAllById((int) humHDAO.count() - 2).getTime(), humHDAO.findAllById((int) humHDAO.count() - 1).getTime(),
//                humHDAO.findAllById((int) humHDAO.count()).getTime()
//        };
//        String[] humHV = new String[] { humHDAO.findAllById((int) humHDAO.count() - 19).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 18).getValue(), humHDAO.findAllById((int) humHDAO.count() - 17).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 16).getValue(), humHDAO.findAllById((int) humHDAO.count() - 15).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 14).getValue(), humHDAO.findAllById((int) humHDAO.count() - 13).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 12).getValue(), humHDAO.findAllById((int) humHDAO.count() - 11).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 10).getValue(), humHDAO.findAllById((int) humHDAO.count() - 9).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 8).getValue(), humHDAO.findAllById((int) humHDAO.count() - 7).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 6).getValue(), humHDAO.findAllById((int) humHDAO.count() - 5).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 4).getValue(), humHDAO.findAllById((int) humHDAO.count() - 3).getValue(),
//                humHDAO.findAllById((int) humHDAO.count() - 2).getValue(), humHDAO.findAllById((int) humHDAO.count() - 1).getValue(),
//                humHDAO.findAllById((int) humHDAO.count()).getValue()
//        };
//        m.addAttribute("humHT", humHT);
//        m.addAttribute("humHV", humHV);
//        String[] humST = new String[] { humSDAO.findAllById((int) humSDAO.count() - 19).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 18).getTime(), humSDAO.findAllById((int) humSDAO.count() - 17).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 16).getTime(), humSDAO.findAllById((int) humSDAO.count() - 15).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 14).getTime(), humSDAO.findAllById((int) humSDAO.count() - 13).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 12).getTime(), humSDAO.findAllById((int) humSDAO.count() - 11).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 10).getTime(), humSDAO.findAllById((int) humSDAO.count() - 9).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 8).getTime(), humSDAO.findAllById((int) humSDAO.count() - 7).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 6).getTime(), humSDAO.findAllById((int) humSDAO.count() - 5).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 4).getTime(), humSDAO.findAllById((int) humSDAO.count() - 3).getTime(),
//                humSDAO.findAllById((int) humSDAO.count() - 2).getTime(), humSDAO.findAllById((int) humSDAO.count() - 1).getTime(),
//                humSDAO.findAllById((int) humSDAO.count()).getTime()
//        };
//        String[] humSV = new String[] { humSDAO.findAllById((int) humSDAO.count() - 19).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 18).getValue(), humSDAO.findAllById((int) humSDAO.count() - 17).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 16).getValue(), humSDAO.findAllById((int) humSDAO.count() - 15).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 14).getValue(), humSDAO.findAllById((int) humSDAO.count() - 13).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 12).getValue(), humSDAO.findAllById((int) humSDAO.count() - 11).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 10).getValue(), humSDAO.findAllById((int) humSDAO.count() - 9).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 8).getValue(), humSDAO.findAllById((int) humSDAO.count() - 7).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 6).getValue(), humSDAO.findAllById((int) humSDAO.count() - 5).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 4).getValue(), humSDAO.findAllById((int) humSDAO.count() - 3).getValue(),
//                humSDAO.findAllById((int) humSDAO.count() - 2).getValue(), humSDAO.findAllById((int) humSDAO.count() - 1).getValue(),
//                humSDAO.findAllById((int) humSDAO.count()).getValue()
//        };
//        m.addAttribute("humST", humST);
//        m.addAttribute("humSV", humSV);
//        String[] presHT = new String[] { presHDAO.findAllById((int) presHDAO.count() - 19).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 18).getTime(), presHDAO.findAllById((int) presHDAO.count() - 17).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 16).getTime(), presHDAO.findAllById((int) presHDAO.count() - 15).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 14).getTime(), presHDAO.findAllById((int) presHDAO.count() - 13).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 12).getTime(), presHDAO.findAllById((int) presHDAO.count() - 11).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 10).getTime(), presHDAO.findAllById((int) presHDAO.count() - 9).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 8).getTime(), presHDAO.findAllById((int) presHDAO.count() - 7).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 6).getTime(), presHDAO.findAllById((int) presHDAO.count() - 5).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 4).getTime(), presHDAO.findAllById((int) presHDAO.count() - 3).getTime(),
//                presHDAO.findAllById((int) presHDAO.count() - 2).getTime(), presHDAO.findAllById((int) presHDAO.count() - 1).getTime(),
//                presHDAO.findAllById((int) presHDAO.count()).getTime()
//        };
//        String[] presHV = new String[] { presHDAO.findAllById((int) presHDAO.count() - 19).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 18).getValue(), presHDAO.findAllById((int) presHDAO.count() - 17).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 16).getValue(), presHDAO.findAllById((int) presHDAO.count() - 15).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 14).getValue(), presHDAO.findAllById((int) presHDAO.count() - 13).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 12).getValue(), presHDAO.findAllById((int) presHDAO.count() - 11).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 10).getValue(), presHDAO.findAllById((int) presHDAO.count() - 9).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 8).getValue(), presHDAO.findAllById((int) presHDAO.count() - 7).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 6).getValue(), presHDAO.findAllById((int) presHDAO.count() - 5).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 4).getValue(), presHDAO.findAllById((int) presHDAO.count() - 3).getValue(),
//                presHDAO.findAllById((int) presHDAO.count() - 2).getValue(), presHDAO.findAllById((int) presHDAO.count() - 1).getValue(),
//                presHDAO.findAllById((int) presHDAO.count()).getValue()
//        };
//        m.addAttribute("presHT", presHT);
//        m.addAttribute("presHV", presHV);
//        String[] presST = new String[] { presSDAO.findAllById((int) presSDAO.count() - 19).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 18).getTime(), presSDAO.findAllById((int) presSDAO.count() - 17).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 16).getTime(), presSDAO.findAllById((int) presSDAO.count() - 15).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 14).getTime(), presSDAO.findAllById((int) presSDAO.count() - 13).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 12).getTime(), presSDAO.findAllById((int) presSDAO.count() - 11).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 10).getTime(), presSDAO.findAllById((int) presSDAO.count() - 9).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 8).getTime(), presSDAO.findAllById((int) presSDAO.count() - 7).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 6).getTime(), presSDAO.findAllById((int) presSDAO.count() - 5).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 4).getTime(), presSDAO.findAllById((int) presSDAO.count() - 3).getTime(),
//                presSDAO.findAllById((int) presSDAO.count() - 2).getTime(), presSDAO.findAllById((int) presSDAO.count() - 1).getTime(),
//                presSDAO.findAllById((int) presSDAO.count()).getTime()
//        };
//        String[] presSV = new String[] { presSDAO.findAllById((int) presSDAO.count() - 19).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 18).getValue(), presSDAO.findAllById((int) presSDAO.count() - 17).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 16).getValue(), presSDAO.findAllById((int) presSDAO.count() - 15).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 14).getValue(), presSDAO.findAllById((int) presSDAO.count() - 13).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 12).getValue(), presSDAO.findAllById((int) presSDAO.count() - 11).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 10).getValue(), presSDAO.findAllById((int) presSDAO.count() - 9).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 8).getValue(), presSDAO.findAllById((int) presSDAO.count() - 7).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 6).getValue(), presSDAO.findAllById((int) presSDAO.count() - 5).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 4).getValue(), presSDAO.findAllById((int) presSDAO.count() - 3).getValue(),
//                presSDAO.findAllById((int) presSDAO.count() - 2).getValue(), presSDAO.findAllById((int) presSDAO.count() - 1).getValue(),
//                presSDAO.findAllById((int) presSDAO.count()).getValue()
//        };
//        m.addAttribute("presST", presST);
//        m.addAttribute("presSV", presSV);
        return "comparison.html";
    }
}





