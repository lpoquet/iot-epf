package capteurs;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableJpaRepositories
@EnableAutoConfiguration
@SpringBootConfiguration
@EnableSwagger2
@ComponentScan(basePackages = {"capteurs"})

public class Application {

    public static void main(String[] args) {
        new SpringApplicationBuilder(capteurs.Application.class)
                .run();
    }
}
