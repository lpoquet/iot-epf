package capteurs.DAO;

import capteurs.model.PressionHome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PressionHomeDAO extends JpaRepository<PressionHome, Integer> {

    PressionHome findAllById(int id);
}
