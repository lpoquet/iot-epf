package capteurs.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import capteurs.model.Humidite;

@Repository
public interface HumiditeDAO extends JpaRepository<Humidite, Integer> {

    Humidite findAllById(int id);
}
