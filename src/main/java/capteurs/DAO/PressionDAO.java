package capteurs.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import capteurs.model.Pression;

@Repository
public interface PressionDAO extends JpaRepository<Pression, Integer> {

    Pression findAllById(int id);
}
