package capteurs.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import capteurs.model.Temperature;

@Repository
public interface TemperatureDAO extends JpaRepository<Temperature, Integer> {

  Temperature findAllById(int id);
}
