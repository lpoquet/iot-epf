package capteurs.DAO;

import capteurs.model.HumiditeHome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HumiditeHomeDAO extends JpaRepository<HumiditeHome, Integer> {

    HumiditeHome findAllById(int id);
}
