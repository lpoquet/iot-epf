package capteurs.DAO;

import capteurs.model.TemperatureSchool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemperatureSchoolDAO extends JpaRepository<TemperatureSchool, Integer> {

  TemperatureSchool findAllById(int id);
}
