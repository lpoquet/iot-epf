package capteurs.DAO;

import capteurs.model.TemperatureHome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemperatureHomeDAO extends JpaRepository<TemperatureHome, Integer> {

  TemperatureHome findAllById(int id);
}
