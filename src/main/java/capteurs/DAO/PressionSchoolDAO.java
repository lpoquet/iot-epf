package capteurs.DAO;

import capteurs.model.PressionSchool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PressionSchoolDAO extends JpaRepository<PressionSchool, Integer> {

    PressionSchool findAllById(int id);
}
