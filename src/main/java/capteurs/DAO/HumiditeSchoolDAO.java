package capteurs.DAO;

import capteurs.model.HumiditeSchool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HumiditeSchoolDAO extends JpaRepository<HumiditeSchool, Integer> {

    HumiditeSchool findAllById(int id);
}
