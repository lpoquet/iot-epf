# Projet iot
=====

Mikael ARCHAS
Eva BROSSEAU
Roxane MERLEN 
Ludivine POQUET
Roxane WALLEZ 

# Code capteur :

```
import time
import calendar
import board
import mysql.connector
from busio import I2C
import adafruit_bme680

# Create library object using our Bus I2C port
i2c = I2C(board.SCL, board.SDA)
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, debug=False)

conn = mysql.connector.connect(host="localhost",user="root",password="toor", database="defaultdb")
cursor = conn.cursor()

# change this to match the location's pressure (hPa) at sea level
bme680.sea_level_pressure = 1013.25
i = 0
while True:
        ts = calendar.timegm(time.gmtime())
        print("\nTemperature: %0.1f C" % bme680.temperature)
        a = bme680.temperature
        
        print("Humidity: %0.1f %%" % bme680.humidity)
        b = bme680.humidity
        
        print("Pressure: %0.3f hPa" % bme680.pressure)
        c = bme680.pressure
    
        cursor.execute("""INSERT INTO temperature (time, value) VALUES (%s, %s)""" % (ts, a))
        cursor.execute("""INSERT INTO humidite (time, value) VALUES (%s, %s)""" % (ts, b))
        cursor.execute("""INSERT INTO pression (time, value) VALUES (%s, %s)""" % (ts, c))
        conn.commit()
        time.sleep(1)

conn.close()
```
